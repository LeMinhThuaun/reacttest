import { combineReducers } from 'redux';
import pokemonReducer from './AppChildren/reducer';

const allReducers = combineReducers({
    pokemonReducer,
});
export default allReducers;
// export default allSaga;
import { all } from "redux-saga/effects";
import appSaga from "./AppChildren/saga";

function* allSaga() {
    yield all([
        appSaga(),
    ]);
}

export default allSaga;
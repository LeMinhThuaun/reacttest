// import actionsPoke from './action'

let posts = {
};

export default function pokemonReducer(state = posts, action) {
    // console.log("đã tới reducer", action.params)
    switch (action.type) {

        case 'POKEMON_LIST_REQUEST':
            return {
                ...state,
                listPokemon: action.fakedata,
            };
        case 'POKEMON_LIST_SUCCESS':
            return {
                ...state,
                listPokemon: action.fakedata,
            };
        case 'POKEMON_LIST_FAILURE':
            return {
                ...state,
                listPokemon: action.fakedata,
            };

        //add
        case 'POKEMON_ADD_REQUEST':
            return {
                ...state,
                listPokemon: action.fakedata,
            };
        case 'POKEMON_ADD_SUCCESS':
            return {
                ...state,
                listPokemon: action.fakedata,
            };
        case 'POKEMON_ADD_FAILURE':
            return {
                ...state,
                listPokemon: action.fakedata,
            };
        default:
            return state;
    }
}
const actionsPoke = {

    POKEMON_LIST_REQUEST: 'POKEMON_LIST_REQUEST',
    POKEMON_LIST_FAILURE: 'POKEMON_LIST_FAILURE',
    POKEMON_LIST_SUCCESS: 'POKEMON_LIST_SUCCESS',
    
    POKEMON_ADD_REQUEST: 'POKEMON_ADD_REQUEST',
    POKEMON_ADD_FAILURE: 'POKEMON_ADD_FAILURE',
    POKEMON_ADD_SUCCESS: 'POKEMON_ADD_SUCCESS',

    loadListPokemon: (params) => ({
        type: actionsPoke.POKEMON_LIST_REQUEST,
        params: params,
    }),

    addPokemon: (params) => ({
        type: actionsPoke.POKEMON_ADD_REQUEST,
        params: params,
    }),

};
export default actionsPoke;
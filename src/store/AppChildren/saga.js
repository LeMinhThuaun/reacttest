import { put, takeLatest, all } from 'redux-saga/effects';
import actionsPoke from './action'
import fakedata from '../../data/fakeData'

function* getPoke(payload) {
    // console.log(payload);
    try {
        if (payload.params == true) {
            yield put({ type: actionsPoke.POKEMON_LIST_SUCCESS, fakedata })
        }
        else {
            yield put({ type: actionsPoke.POKEMON_LIST_FAILURE,fakedata: []  })
        }

    } catch (e) {

        yield put({ type: actionsPoke.POKEMON_LIST_FAILURE, e });

    }

}

export default function* rootSaga() {
    yield all([
        takeLatest('POKEMON_LIST_REQUEST', getPoke),    //
    ]);
}

import { Button, Input } from 'antd';
import { Form } from 'antd';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { actionsPoke } from './store/action';

const AppChildren = () => {

    const dispatch = useDispatch()
    const [data, setData] = useState(null);
    const [checkBtn, setCheckBtn] = useState(true);
    const [formAdd] = Form.useForm()
    const store = useSelector(state => state.pokemonReducer);
    const { listPokemon } = store   //store đã lưu data 1 lần và vẫn cứ ở đó

    useEffect(() => {
        if (listPokemon) {
            setData(listPokemon);
        }
    }, [listPokemon])

    useEffect(() => {
        if (checkBtn) {
            console.log(checkBtn);
        }
    }, [checkBtn])


    const clickGo = () => {

        setCheckBtn(!checkBtn);
        dispatch(actionsPoke.loadListPokemon(checkBtn));

    }

    const submitForm = (e) => {

        const { poke_name, pokemon_id } = e;
        console.log(e);
        const array = []

        array.push(e)

        const array1 = data.concat(array);
        setData(array1);

        // dispatch(actionsPoke.addPokemon({ pokemon_id: pokemon_id, poke_name: poke_name }));
    }
    return (
        <div className="test">
            <div className="test">
                {
                    checkBtn ?

                        <button onClick={clickGo}>Show</button>
                        :
                        <>
                            <Form
                                form={formAdd}
                                onFinish={submitForm}
                            >
                                <Form.Item name="pokemon_id" label="ID">
                                    <Input />
                                </Form.Item>

                                <Form.Item name="poke_name" label="Name">
                                    <Input></Input>
                                </Form.Item>

                                <Button htmlType='submit'>Thêm</Button>
                            </Form>

                            <button onClick={clickGo}>Hide</button>
                        </>
                }

            </div>
            {

                data && data.length && data.map((value, key) => {
                    return (
                        <div key={key}>
                            <h5>{value.pokemon_id}</h5>
                            <span>{value.poke_name}</span>
                        </div>
                    )
                })
            }
        </div>
    );
}

export default AppChildren;
